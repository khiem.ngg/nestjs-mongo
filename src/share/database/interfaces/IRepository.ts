import { IPaginateParams } from './IPagination';
export interface IFindByIdAndPopulateParams {
  id: string;
  populate: any;
  projections?: any;
}

export interface IFindByConditionAndPopulateParams {
  conditions: any;
  populate: any;
  projections?: any;
}

export interface IListParams {
  conditions?: any;
  projections?: any;
  paginate?: IPaginateParams;
  populate?: any;
}

export interface IFindByIdAndUpdateParams {
  id: string;
  data: any;
  options?: any;
}

export interface IUpdateByConditionParams {
  conditions: any;
  data: any;
  options?: any;
}
