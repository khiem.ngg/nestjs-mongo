import { Document, FilterQuery, ProjectionType, QueryOptions, UpdateQuery } from 'mongoose';
import { IPaginateParams } from './IPagination';
export interface IFindByIdAndPopulateParams<T extends Document> {
  id: string;
  populate: any;
  projections?: ProjectionType<T>;
}

export interface IFindByConditionAndPopulateParams<T extends Document> {
  conditions: FilterQuery<T>;
  populate: any;
  projections?: ProjectionType<T>;
}

export interface IListParams<T extends Document> {
  conditions?: FilterQuery<T>;
  projections?: ProjectionType<T>;
  paginate?: IPaginateParams;
  populate?: any;
}

export interface IFindByIdAndUpdateParams<T extends Document> {
  id: string;
  data: UpdateQuery<T>;
  options?: QueryOptions<T>;
}

export interface IUpdateByConditionParams<T extends Document> {
  conditions: FilterQuery<T>;
  data: UpdateQuery<T>;
  options?: QueryOptions<T>;
}
