import { Document, FilterQuery, ProjectionType, QueryOptions } from 'mongoose';
import { SoftDeleteModel } from 'mongoose-delete';
import {
  IFindByConditionAndPopulateParams,
  IFindByIdAndPopulateParams,
  IFindByIdAndUpdateParams,
  IListParams,
  IUpdateByConditionParams,
} from '../interfaces/IMongoRepository';

export class BaseRepository<T extends Document> {
  constructor(private _model: SoftDeleteModel<T>) {}

  async create(params: T): Promise<T> {
    return new this._model(params).save();
  }

  async createMultiple(params: T[]): Promise<T[]> {
    return this._model.insertMany(params);
  }

  async findById(id: string, projections?: ProjectionType<T>): Promise<T> {
    return this._model.findById(id, projections || {}).exec();
  }

  async findByCondition(conditions: FilterQuery<T>, projections?: ProjectionType<T>): Promise<T> {
    return this._model.findOne(conditions, projections || {}).exec();
  }

  async findByConditionLean(conditions: FilterQuery<T>, projections?: ProjectionType<T>) {
    return this._model
      .findOne(conditions, projections || {})
      .lean()
      .exec();
  }

  async findByIdAndPopulate(params: IFindByIdAndPopulateParams<T>) {
    return this._model
      .findById(params.id, params.projections || {})
      .populate(params.populate)
      .exec();
  }

  async findByConditionAndPopulate(params: IFindByConditionAndPopulateParams<T>) {
    return this._model
      .findOne(params.conditions, params.projections || {})
      .populate(params.populate)
      .exec();
  }

  async list(params: Omit<IListParams<T>, 'populate'>) {
    const paginateParams = params.paginate;

    const page = paginateParams?.page && paginateParams.page > 0 ? Number(paginateParams.page) : 1;
    const pageSize = paginateParams?.pageSize && paginateParams.pageSize > 0 ? Number(paginateParams.pageSize) : 20;
    const queryOptions: QueryOptions<T> = {
      limit: pageSize,
      skip: (page - 1) * pageSize,
      sort: paginateParams?.sortBy
        ? {
            [paginateParams.sortBy.trim()]: paginateParams?.sortOrder == 'desc' ? -1 : 1,
          }
        : undefined,
    };

    const totalDocuments = await this.countByCondition(params.conditions);
    const totalPage = Math.ceil(totalDocuments / pageSize);
    const result: T[] = await this._model.find(params.conditions || {}, params.projections, queryOptions);

    return {
      data: result,
      total: totalDocuments,
      page: page,
      pageSize: pageSize,
      totalPage: totalPage,
    };
  }

  async listAndPopulate(params: IListParams<T>) {
    const paginateParams = params.paginate;

    const page = paginateParams?.page && paginateParams.page > 0 ? Number(paginateParams.page) : 1;
    const pageSize = paginateParams?.pageSize && paginateParams.pageSize > 0 ? Number(paginateParams.pageSize) : 20;
    const queryOptions: QueryOptions<T> = {
      limit: pageSize,
      skip: (page - 1) * pageSize,
      sort: paginateParams?.sortBy
        ? {
            [paginateParams.sortBy.trim()]: paginateParams?.sortOrder == 'desc' ? -1 : 1,
          }
        : undefined,
    };

    const totalDocuments = await this.countByCondition(params.conditions);
    const totalPage = Math.ceil(totalDocuments / pageSize);
    const result: T[] = await this._model
      .find(params.conditions || {}, params.projections, queryOptions)
      .populate(params.populate);

    return {
      data: result,
      total: totalDocuments,
      page: page,
      pageSize: pageSize,
      totalPage: totalPage,
    };
  }

  async listAll(params: Omit<IListParams<T>, 'paginate' | 'populate'>): Promise<T[]> {
    return this._model.find(params.conditions || {}, params.projections || {});
  }

  async listAllAndPopulate(params: Omit<IListParams<T>, 'paginate'>): Promise<T[]> {
    return this._model.find(params.conditions || {}, params.projections || {}).populate(params.populate);
  }

  async findByIdAndUpdate(params: IFindByIdAndUpdateParams<T>) {
    return this._model
      .findByIdAndUpdate(params.id, params.data, Object.assign(params.options || {}, { new: true }))
      .exec();
  }

  async findOneAndUpdate(params: IUpdateByConditionParams<T>) {
    return this._model
      .findOneAndUpdate(params.conditions, params.data, Object.assign(params.options || {}, { new: true }))
      .exec();
  }

  async updateOne(params: IUpdateByConditionParams<T>) {
    return this._model.updateOne(params.conditions, params.data, params.options || {}).exec();
  }

  async updateMany(params: IUpdateByConditionParams<T>) {
    return this._model.updateMany(params.conditions, params.data, params.options || {}).exec();
  }

  async findByIdAndDelete(id: string) {
    return this._model.findByIdAndDelete(id).exec();
  }

  async findOneAndDelete(conditions: FilterQuery<T>) {
    return this._model.findOneAndDelete(conditions || {}).exec();
  }

  async deleteOne(conditions: FilterQuery<T>) {
    return this._model.deleteOne(conditions || {}).exec();
  }

  async deleteMany(conditions: FilterQuery<T>) {
    return this._model.deleteMany(conditions || {}).exec();
  }

  async countByCondition(conditions: FilterQuery<T>) {
    return this._model.countDocuments(conditions || {});
  }

  async aggregate(conditions: any[]) {
    return this._model.aggregate(conditions);
  }

  // Methods for soft delete
  async softDeleteById(id: string) {
    return this._model.deleteById(id);
  }

  async softDeleteByCondition(conditions: FilterQuery<T>) {
    return this._model.delete(conditions);
  }

  async restoreById(id: string) {
    return this._model.restore({ _id: id });
  }

  async restoreByCondition(conditions: FilterQuery<T>) {
    return this._model.restore(conditions);
  }
}
