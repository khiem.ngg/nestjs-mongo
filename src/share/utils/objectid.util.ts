import { BadRequestException } from '@nestjs/common';
import { ObjectId } from 'mongodb';

export function toObjectId({ value, key }): ObjectId {
  if (ObjectId.isValid(value) && new ObjectId(value).toString() === value) {
    return new ObjectId(value);
  } else {
    throw new BadRequestException(`${key} is invalid ObjectId`);
  }
}

export function toArrayObjectId({ value, key }): ObjectId[] {
  const stringError: string[] = [];
  (value as string[]).forEach((stringId) => {
    if (!ObjectId.isValid(stringId)) stringError.push(stringId);
  });
  if (stringError.length) {
    throw new BadRequestException([
      `${key} is invalid ObjectId array`,
      ...stringError.map((item) => `${item} is invalid ObjectId`),
    ]);
  }
  return (value as string[]).map((stringId) => ObjectId.createFromHexString(stringId.toString()));
}

export function stringIsObjectId({ value, key }): ObjectId {
  if (ObjectId.isValid(value)) {
    return value;
  } else {
    throw new BadRequestException(`${key} is invalid ObjectId`);
  }
}

export function stringsIsArrayObjectId({ value, key }): ObjectId[] {
  const stringError: string[] = [];
  (value as string[]).forEach((stringId) => {
    if (!ObjectId.isValid(stringId)) stringError.push(stringId);
  });
  if (stringError.length) {
    throw new BadRequestException([
      `${key} is invalid ObjectId array`,
      ...stringError.map((item) => `${item} is invalid ObjectId`),
    ]);
  }
  return value;
}
