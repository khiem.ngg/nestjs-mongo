import { ApiParamOptions } from '@nestjs/swagger';

export function swaggerSchemaExample(example, description) {
  return {
    content: {
      schema: {
        example,
      },
    },
    description,
  };
}

export const swaggerObjectIdParam: ApiParamOptions = {
  name: 'id',
  type: String,
  required: true,
  description: 'string can be convert to ObjectId',
};

export const swaggerCommonReponse = {
  INVALID_OBJECTID: swaggerSchemaExample(
    {
      path: '/api/v1/files/6358b109d86cc',
      message: 'Validation failed (ObjectId is expected)',
      code: 'sys00001',
      statusCode: 400,
    },
    'Invalid ObjectId',
  ),
};
