import { Injectable, Logger } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { UPLOAD_FILE_CONFIG } from './../../configs/constant.config';

@Injectable()
export class AwsS3Service {
  private s3: S3;
  private readonly logger = new Logger(AwsS3Service.name);

  constructor() {
    this.s3 = new S3({
      accessKeyId: UPLOAD_FILE_CONFIG.s3AccessKey,
      secretAccessKey: UPLOAD_FILE_CONFIG.s3SecretKey,
      // needed with minio?
      endpoint: process.env.S3_MINIO_DOMAIN,
      s3ForcePathStyle: true,
      signatureVersion: 'v4',
    });
  }

  async uploadFile(file: Express.Multer.File, folder?: string, name?: string) {
    const fileExtension = file.originalname.substring(file.originalname.lastIndexOf('.') + 1, file.originalname.length);
    const fileName: string = name ? `${name}.${fileExtension}` : `${Date.now()}-${file.originalname}`;
    try {
      const uploadResult = await this.s3
        .upload({
          Bucket: UPLOAD_FILE_CONFIG.s3BucketName,
          Key: folder ? `${folder}/${fileName}` : `${fileName}`,
          Body: file.buffer,
          ContentType: file.mimetype,
        })
        .promise();
      return uploadResult;
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async deleteFile(fileKey: string): Promise<void> {
    try {
      await this.s3
        .deleteObject({
          Bucket: UPLOAD_FILE_CONFIG.s3BucketName,
          Key: fileKey,
        })
        .promise();
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async deleteMultipleFiles(fileKeys: string[]): Promise<void> {
    const objects = fileKeys.map((key) => ({ Key: key }));
    try {
      await this.s3
        .deleteObjects({
          Bucket: UPLOAD_FILE_CONFIG.s3BucketName,
          Delete: {
            Objects: objects,
          },
        })
        .promise();
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }
}
