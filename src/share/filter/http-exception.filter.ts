import { Catch, ArgumentsHost, HttpException, ExceptionFilter, HttpStatus, Logger } from '@nestjs/common';
import { Request, Response } from 'express';
import { ERROR } from '../common/error-code.const';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(HttpExceptionFilter.name);

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;
    const message = exception.message || ERROR.COMMON_SYSTEM_ERROR.MESSAGE;

    if (status == HttpStatus.INTERNAL_SERVER_ERROR) {
      const thisLog = {
        path: request.path,
        ipAddress: request.headers['x-forwarded-for'] || request.ip,
        method: request.method,
        error: exception,
      };
      const urlMessage = `URL: ${request.path}, MESSAGE: ${message}`;
      this.logger.error(urlMessage, exception.stack, thisLog);
    }

    response.status(status).json({
      path: request.url,
      message: status == HttpStatus.INTERNAL_SERVER_ERROR ? ERROR.COMMON_SYSTEM_ERROR.MESSAGE : message,
      code:
        exception instanceof HttpException && exception.getResponse()['code']
          ? exception.getResponse()['code']
          : ERROR.COMMON_SYSTEM_ERROR.CODE,
      errors: exception instanceof HttpException ? exception.getResponse()['errors'] : undefined,
      statusCode: status,
    });
  }
}
