import { BaseRepository } from './../../share/database/mongodb/base.repository';
import { Injectable } from '@nestjs/common';
import { Entity, EntityDocument } from './schemas/entity.schema';
import { InjectModel } from '@nestjs/mongoose';
import { SoftDeleteModel } from 'mongoose-delete';

@Injectable()
export class EntityRepository extends BaseRepository<EntityDocument> {
  constructor(@InjectModel(Entity.name) private entityModel: SoftDeleteModel<EntityDocument>) {
    super(entityModel);
  }
}
