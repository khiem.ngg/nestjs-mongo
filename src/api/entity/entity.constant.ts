import { swaggerSchemaExample } from '../../share/utils';

export const ENTITY_CONST = {
  MODEL_NAME: 'entities',
};

export const FILE_SWAGGER_RESPONSE = {
  UPLOAD_SUCCESS: swaggerSchemaExample(
    {
      data: {
        key: '1666756873711-README.md',
        url: 'http://localhost:9000/file-service/1666756873711-README.md',
        bucket: 'file-service',
        _id: '6358b109d86cc5d26264611c',
        deleted: false,
        createdAt: '2022-10-26T04:01:13.830Z',
        updatedAt: '2022-10-26T04:01:13.830Z',
        __v: 0,
      },
    },
    'Upload file successfully',
  ),

  GET_URL_SUCCESS: swaggerSchemaExample(
    {
      data: {
        url: 'http://localhost:9000/file-service/1666756873711-README.md',
      },
    },
    'Get file url successfully',
  ),

  DELETE_SUCCESS: swaggerSchemaExample(
    {
      data: {
        success: true,
        deleted_count: 1,
      },
    },
    'Delete file successfully',
  ),

  FILE_NOT_FOUND: swaggerSchemaExample(
    {
      path: '/api/v1/files/6358b109d86cc5d26264611c',
      message: 'File does not exist',
      code: 'sys00001',
      statusCode: 404,
    },
    'File does not exist',
  ),

  FILE_IDS_INVALID: swaggerSchemaExample(
    {
      path: '/api/v1/files',
      message: 'Unprocessable Entity',
      code: 'sys00001',
      errors: ['ids is invalid ObjectId array', '6358af7e76104848c0b7978 is invalid ObjectId'],
      statusCode: 422,
    },
    'strings ObjectId array invalid',
  ),
};
